import pyparsing as pp

shape = pp.Opt(pp.Group(pp.delimitedList(
    pp.Keyword("orthogonal") |
    pp.Keyword("vertical") |
    pp.Keyword("horizontal") |
    pp.Keyword("diagonal") |
    pp.Keyword("square") | pp.Keyword("all") |
    pp.Keyword("rectangle") |
    pp.Keyword("circle") |
    pp.Group(pp.Keyword("vector") + pp.Suppress("-") +
             pp.Group(pp.common.integer + pp.Suppress(":") + pp.common.integer), aslist=True)
    , delim="/").set_name("shape")), [])

index_range = (pp.common.integer + pp.Suppress("-") + pp.common.integer).set_parse_action(
    lambda t: list(range(t[0], t[1] + 1)))
indexes = pp.Opt(pp.Group(pp.delimitedList((index_range | pp.common.signed_integer), delim="/").set_name("indexes"), aslist=True), [])

direction = pp.Opt(pp.Group(pp.delimitedList(
    pp.Keyword("leftward") |
    pp.Keyword("left") |
    pp.Keyword("rightward") |
    pp.Keyword("right") |
    pp.Keyword("upward") |
    pp.Keyword("up") |
    pp.Keyword("forward") |
    pp.Keyword("downward") |
    pp.Keyword("down") |
    pp.Keyword("backward")
    , delim="/").set_name("direction")), [])


def tile_list_parser(separator: str = "/", optional: bool = False):
    parser = pp.Group(pp.delimited_list(pp.Word(pp.alphanums + "-") | "any", delim=separator), aslist=True).set_name("tile-list")
    return pp.Opt(parser, default="") if optional else parser


tile_list = tile_list_parser(optional=True)

base_tile = pp.Opt((pp.Keyword("on-base").suppress() + tile_list).set_name("base-tile"), [])
stop_tile = pp.Opt((pp.Keyword("stop-on").suppress() + tile_list).set_name("stop-tile"), [])

identifier = pp.Opt((pp.Suppress("[") + pp.Word(pp.alphanums + "-") + pp.Suppress("]")).set_name("identifier"), "")

traversal = (
        pp.Keyword("on").suppress() +
        shape("shape") +
        direction("direction") +
        indexes("indexes") +
        base_tile("base-tile") +
        stop_tile("stop-tile") +
        identifier("identifier")
).set_name("traversal")


traversal.create_diagram('parsertest.html')

print(str(pp.Opt(pp.Word(pp.alphanums), default="")("test").parse_string("")))
print(traversal.parse_string("on"))
print(traversal.parse_string("on orthogonal left on-base any"))
print(traversal.parse_string("on vector-1:2/vector-2:1 1"))
print(traversal.parse_string("on diagonal rightward/forward 1-5"))
print(traversal.parse_string("on diagonal 1-4/7"))
print(traversal.parse_string("on horizontal 1-3/5 on-base empty [id]"))
test = traversal.parse_string("on horizontal left 1-3/5/8 stop-on tile/empty")
print(test, test.indexes, test.identifier)
print(tile_list.parse_string(""))
