# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging

logger = logging.getLogger("turntile")


class ColorFormatter(logging.Formatter):
    COLOR_PREFIX = {
        "NOTSET": "\x1b[37;20m",
        "DEBUG": "\x1b[34;20m",
        "INFO": "\x1b[32;20m",
        "WARNING": "\x1b[33;1m",
        "ERROR": "\x1b[31;1m",
        "CRITICAL": "\x1b[35;1m"
    }

    def format(self, record: logging.LogRecord) -> str:
        return self.COLOR_PREFIX[record.levelname] + super().format(record) + "\x1b[0m"


def setup_logging(level: str) -> None:
    log = logging.getLogger("turntile")
    log.setLevel(level)

    handler = logging.StreamHandler()
    handler.setLevel(level)
    handler.setFormatter(ColorFormatter("[%(levelname)s] [%(module)s] {%(asctime)s}  %(message)s", datefmt="%H:%M:%S"))

    log.addHandler(handler)
