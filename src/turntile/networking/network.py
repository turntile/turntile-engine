# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from __future__ import annotations  # for type hinting forward references

import socket
import threading
from typing import Any, Final

from turntile.log import logger
from turntile.networking.util import BaseInterfaceHandler, ScreenType, MessageType, encode_payload, PAYLOAD_MAX_SIZE, \
    LargePayloadException, PAYLOAD_BYTE_COUNT, MSG_TYPE_BYTE_COUNT, decode_payload
from turntile.parsing.action import Act
from turntile.parsing.config import Config
from turntile.state import state


def parse_message_type(msg: bytes) -> tuple[MessageType, bytes]:
    msg_type = MessageType(bytes_to_int(msg[:MSG_TYPE_BYTE_COUNT]))
    payload = msg[MSG_TYPE_BYTE_COUNT:]
    return msg_type, payload


def int_to_bytes(value: int, number_of_bytes: int) -> bytes:
    return value.to_bytes(number_of_bytes, "big")  # convert int to network order bytes (big-endian)


def bytes_to_int(value: bytes) -> int:
    return int.from_bytes(value, "big")


class SocketHandler:
    def __init__(self, s: socket.socket) -> None:
        self.s = s

    def __enter__(self) -> SocketHandler:
        return self

    def __exit__(self, *args: object) -> None:
        self.s.__exit__(*args)

    @classmethod
    def connect(cls, host: str, port: int) -> SocketHandler:
        return cls(socket.create_connection((host, port)))

    def send(self, msg_type: MessageType, payload: Any = b"") -> None:
        self.send_bytes(msg_type, encode_payload(msg_type, payload))

    def send_bytes(self, msg_type: MessageType, payload: bytes = b"") -> None:
        msg_type_bytes = int_to_bytes(msg_type.value, MSG_TYPE_BYTE_COUNT)  # type: ignore
        msg_bytes = msg_type_bytes + payload

        if len(payload) > PAYLOAD_MAX_SIZE:
            raise LargePayloadException(len(payload))

        header = int_to_bytes(len(payload), PAYLOAD_BYTE_COUNT)
        self.s.sendall(header + msg_bytes)

    def receive(self) -> tuple[MessageType, bytes]:
        payload_size = self._receive_n_bytes(PAYLOAD_BYTE_COUNT)
        if not payload_size:
            return MessageType.DISCONNECT, b""

        msg = self._receive_n_bytes(MSG_TYPE_BYTE_COUNT + bytes_to_int(payload_size))
        if not msg:
            return MessageType.DISCONNECT, b""

        return parse_message_type(msg)

    def _receive_n_bytes(self, n: int) -> bytes:
        chunks = []
        received = 0
        while received < n:
            chunk = self.s.recv(min(n - received, 4096))
            if not chunk:
                return b""

            chunks.append(chunk)
            received += len(chunk)

        return b"".join(chunks)


def create_server_socket(server_address: tuple[str, int]) -> socket.socket:
    if socket.has_dualstack_ipv6():
        return socket.create_server(server_address, family=socket.AF_INET6, dualstack_ipv6=True)

    return socket.create_server(server_address, family=socket.AF_INET)


def format_address(address_parts: tuple[str, ...]) -> str:
    if len(address_parts) > 2:
        return f"[{address_parts[0]}]:{address_parts[1]}"
    return f"{address_parts[0]}:{address_parts[1]}"


class NetworkHandler:
    def __init__(self) -> None:
        self.lock = threading.Lock()

        # Server connection for client and all client connections for server
        self.connections: list[SocketHandler] = []

        self.handler: BaseInterfaceHandler | None = None

    def start_server(self, port: int, config: Config) -> None:
        state.set_config(config)

        try:
            with create_server_socket(("", port)) as server:
                server.settimeout(5)
                logger.info(f"Server started on port: {port}  Game: {config.name}")
                while True:
                    try:
                        s, address_parts = server.accept()
                        conn = SocketHandler(s)
                        with self.lock:
                            self.connections.append(conn)

                        listen_thread = threading.Thread(target=self._listener, args=(conn, address_parts), daemon=True)
                        listen_thread.start()
                    except TimeoutError:
                        pass

        except KeyboardInterrupt:
            logger.info("Closing server...")
            self.close_server()

    def start_client(self, handler: BaseInterfaceHandler) -> None:
        self.handler = handler
        self.handler.start_client()

    def connect_to_server(self, host: str, port: int, player_name: str | None) -> str | None:
        if self.connections:
            logger.warning("Already connecting to the server")
            return "Connection already established"

        try:
            conn = SocketHandler.connect(host, port)
            with self.lock:
                self.connections.append(conn)

            listen_thread = threading.Thread(target=self._listener, args=(conn, (host, port)), daemon=True)
            listen_thread.start()

            # On new client connection, send player name directly
            if self.handler and player_name is not None:
                conn.send(MessageType.REQUEST_PLAYER_NAME, player_name)

        except Exception as e:
            return f"Couldn't establish connection:\n\n{e}"

        return None

    def close_server(self) -> None:
        with self.lock:
            for conn in self.connections:
                conn.send(MessageType.SHOW_MESSAGE, "Server was closed")
                conn.send(MessageType.DISCONNECT)

            self.connections.clear()

    def send(self, msg_type: MessageType, payload: Any = b"") -> None:
        if self.connections:
            self.connections[0].send(msg_type, payload)
        else:
            logger.warning("No open connections for direct send!")

    def broadcast(self, msg_type: MessageType, payload: Any = b"") -> None:
        if self.connections:
            for connection in self.connections:
                connection.send(msg_type, payload)
        else:
            logger.debug("No connections to broadcast to")

    def remove_connection(self, conn: SocketHandler, address: str) -> None:
        if self.connections:
            self.connections.remove(conn)

        state.client_state.clients.pop(address, None)

    def handle_disconnect(self, conn: SocketHandler, address: str) -> None:
        # Remove this connection from connections list and server client info
        self.remove_connection(conn, address)

        if self.handler:
            # Client goes back to connection screen on disconnect
            self.handler.set_screen(ScreenType.CONNECTION)
        else:
            # Server re-sends client info to other connections
            self.broadcast(MessageType.SET_CLIENTS, state.client_state.clients)

    def _listener(self, conn: SocketHandler, address_parts: tuple[str, ...]) -> None:
        address = format_address(address_parts)
        message_handlers = self.handler if self.handler else ServerHandler(conn, address)

        try:
            with conn:
                logger.info(f"Connected to: {address}")

                while True:
                    msg_type, payload_raw = conn.receive()
                    payload = decode_payload(msg_type, payload_raw)
                    logger.debug(f"{msg_type}, {payload_raw!r}")

                    # lock thread while handling requests
                    with self.lock:
                        # Disconnect handled by both server and client
                        if msg_type == MessageType.DISCONNECT:
                            self.handle_disconnect(conn, address)
                            break  # Breaks out of the listener -> leading to closing of the socket

                        # Other messages directly routed to interface and server handlers
                        if handler_func := getattr(message_handlers, msg_type.name.lower(), None):
                            if handler_func(payload):
                                break  # If True -> breaks out of listener, handler_func must handle disconnect manually

        except Exception as e:
            logger.error("Error: " + str(e))
            with self.lock:
                self.handle_disconnect(conn, address)

        logger.info(f"Connection closed to: {address}")


class ServerHandler:
    def __init__(self, conn: SocketHandler, address: str) -> None:
        self.conn = conn
        self.address = address

    def request_player_name(self, name: str) -> bool:
        # Add new client info and broadcast it to clients
        if not state.client_state.add_client(self.address, name):
            self.conn.send(MessageType.SHOW_MESSAGE, f"A client already connected with the name:\n\n{name}")
            network.remove_connection(self.conn, self.address)
            return True  # Break and close connection/socket if client cannot be added

        self.conn.send(MessageType.INIT_CONFIG, state.config.configuration)
        network.broadcast(MessageType.SET_CLIENTS, state.client_state.clients)

        if not state.started:
            self.conn.send(MessageType.SET_SCREEN, ScreenType.LOBBY)
        else:
            # todo: validate if can connect... only missing player can connect back
            logger.info("Continuing game...")
            network.broadcast(MessageType.OPEN_GAME, name)  # send player name to indicate which player connects
            # todo: synchronize state
            state.open_game(name)

        return False

    def request_client_update(self, client: dict[str, Any]) -> None:
        state.client_state.update_client_info(self.address, client)
        network.broadcast(MessageType.SET_CLIENTS, state.client_state.clients)

    def request_start_game(self, _: Any) -> None:
        if not state.client_state.required_ready():
            self.conn.send(MessageType.SHOW_MESSAGE, "All players are not ready yet!")
            return

        network.broadcast(MessageType.OPEN_GAME, "")
        state.open_game()

    def request_acts(self, act_request: tuple[int, str]) -> None:
        if not state.is_player_turn(self.address):
            logger.debug(f"Not turn for : {state.client_state.clients[self.address]['name']}")
            return

        slot_index, act_string = act_request
        acts = state.allowed_acts.consume(slot_index)

        # validate request
        if acts and act_string == Act.list_as_str(acts):
            state.board.do_acts(acts)
            network.broadcast(MessageType.DO_ACTS, act_string)

    def request_end_turn(self, _: Any) -> None: pass


# Make global network handler object
network: Final[NetworkHandler] = NetworkHandler()
