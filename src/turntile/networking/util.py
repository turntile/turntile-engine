# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from __future__ import annotations  # for type hinting forward references

import json
from abc import ABC, abstractmethod
from enum import Enum, auto
from typing import Any


PAYLOAD_BYTE_COUNT = 3
PAYLOAD_MAX_SIZE = 2 ** (8 * PAYLOAD_BYTE_COUNT) - 1

MSG_TYPE_BYTE_COUNT = 1


# noinspection PyArgumentList
class ScreenType(Enum):
    CONNECTION = auto()
    LOBBY = auto()
    GAME = auto()


class LargePayloadException(Exception):
    def __init__(self, payload_size: int) -> None:
        super().__init__("Payload size exceeded the maximum payload message size: "
                         f"{payload_size} > {PAYLOAD_MAX_SIZE}")


# noinspection PyArgumentList
class MessageType(Enum):
    DISCONNECT = 0, None
    DEBUG = 1, bytes

    # Server messages to client
    INIT_CONFIG = 10, dict
    SET_CLIENTS = 11, dict
    SET_SCREEN = 12, ScreenType
    SHOW_MESSAGE = 13, str
    OPEN_GAME = 14, str
    DO_ACTS = 15, "acts"
    NEXT_TURN = 16, dict

    # Client request messages to server
    REQUEST_PLAYER_NAME = 50, str
    REQUEST_CLIENT_UPDATE = 51, dict
    REQUEST_START_GAME = 52, None
    REQUEST_ACTS = 53, dict
    REQUEST_END_TURN = 54, dict

    # Add datatype attribute for messages
    def __new__(cls, *args: int) -> MessageType:
        obj = object.__new__(cls)
        obj._value_ = args[0]
        obj.datatype = args[1]  # type: ignore
        return obj


def decode_payload(msg_type: MessageType, payload_raw: bytes) -> Any:
    datatype = msg_type.datatype  # type: ignore
    payload = payload_raw.decode("utf-8")
    if datatype == str:
        return payload

    elif datatype == dict or datatype == "acts":
        return json.loads(payload)

    elif isinstance(datatype, type) and issubclass(datatype, Enum):
        return datatype[payload]  # type: ignore

    else:
        return ""


def encode_payload(msg_type: MessageType, payload: Any = b"") -> bytes:
    datatype = msg_type.datatype  # type: ignore
    if datatype == str or datatype == "acts":
        return payload.encode("utf-8")  # type: ignore

    elif datatype == dict:
        return json.dumps(payload, separators=(",", ":")).encode("utf-8")

    elif isinstance(datatype, type) and issubclass(datatype, Enum):
        return payload.name.encode("utf-8")  # type: ignore

    elif type(payload) == bytes:
        return payload
    return b""


class BaseInterfaceHandler(ABC):
    def __init__(self, port: int, player_name: str = "", server_address: str = "", auto_assign: bool = False) -> None:
        self.default_player_name = player_name
        self.default_port = port
        self.auto_connect_address = server_address
        self.auto_player_assign = auto_assign

    @abstractmethod
    def start_client(self) -> None: pass

    @abstractmethod
    def set_screen(self, screen_type: ScreenType) -> None: pass
    @abstractmethod
    def set_clients(self, clients: dict[str, dict[str, Any]]) -> None: pass
    @abstractmethod
    def show_message(self, message: str) -> None: pass
    @abstractmethod
    def init_config(self, configuration: dict[str, Any]) -> None: pass
    @abstractmethod
    def open_game(self, player_name: str) -> None: pass
    @abstractmethod
    def do_acts(self, update: dict[str, Any]) -> None: pass
    @abstractmethod
    def next_turn(self, ply: int) -> None: pass
