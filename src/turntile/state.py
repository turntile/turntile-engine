# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from __future__ import annotations  # for type hinting forward references

from dataclasses import dataclass, astuple, field
from typing import Any, Final, Protocol

from turntile.log import logger
from turntile.parsing.action import ActGroup, Act, get_slot_level, BaseAction, parse_actions
from turntile.parsing.config import Config, TileData
from turntile.parsing.board import BoardConfig


@dataclass
class PlayerInfo:
    number: int = -1
    name: str = ""
    tile: str = ""
    address: str = ""


@dataclass
class ClientState:
    # Clients info (address, player number/tile and ready status)
    clients: dict[str, dict[str, Any]] = field(default_factory=dict)
    player_tiles: list[str] = field(default_factory=list)

    def get_players_info(self, player: PlayerInfo) -> tuple[list[PlayerInfo], PlayerInfo]:
        info: list[PlayerInfo] = []
        for address, client_info in self.clients.items():
            player_number = self.player_tiles.index(client_info["tile"])
            player_info = PlayerInfo(player_number, client_info["name"], client_info["tile"], address)
            info.append(player_info)

            if address == player.address:
                player = player_info

        info.sort(key=lambda p: p.number)
        return info, player

    def client_name_exists(self, name: str) -> bool:
        for info in self.clients.values():
            if info["name"] == name:
                return True
        return False

    def add_client(self, address: str, name: str) -> bool:
        if self.client_name_exists(name) or name == "":
            return False
        self.clients[address] = {"name": name, "tile": "", "ready": False}
        return True

    def required_ready(self) -> bool:
        ready_clients = len([info for info in self.clients.values() if info["ready"] and info["tile"] != ""])
        return ready_clients == len(self.player_tiles) and ready_clients == len(self.clients)

    def current_assignable_tiles(self, selected_tile: str) -> list[str]:
        # Tiles selected by players
        other_assigned_tiles = [info["tile"] for info in self.clients.values()]

        # defined in yaml and not selected by others
        tiles = [tile for tile in self.player_tiles if tile not in other_assigned_tiles]
        if selected_tile != "":
            tiles.append("")  # Add "none/default" tile (if default not selected)
        return tiles

    def update_client_info(self, address: str, client: dict[str, Any]) -> None:
        # Validate client info
        requested_tile = client.get("tile", None)
        current_tile = self.clients[address]["tile"]
        assignable_tiles = self.current_assignable_tiles(current_tile)
        if ((current_tile == "" and not requested_tile) or
                (requested_tile and (requested_tile not in assignable_tiles or requested_tile == ""))):
            client["tile"] = ""
            client["ready"] = False

        # Update client info
        for info_k, value in client.items():
            self.clients[address][info_k] = value


class State:
    def __init__(self) -> None:
        self.client_state: ClientState = ClientState()

        # List of players currently in game. Doesn't automatically update if client disconnects
        self.players_in_game: list[PlayerInfo] = []

        self.ply = 0
        self.started = False

        self.player: PlayerInfo = PlayerInfo()
        self.current_player: PlayerInfo = PlayerInfo()

        self.config: Config = Config()
        self.board: BoardState = BoardState(None)

        self.actions: list[BaseAction] = []

        self.allowed_acts: ActGroup = ActGroup()

    def open_game(self, continuing_player: str | None = None) -> None:
        if not self.started:
            self.started = True
            self.players_in_game, self.player = self.client_state.get_players_info(self.player)
            self.current_player = self.players_in_game[self.ply]

        elif continuing_player:
            # continue game
            pass

    def reset(self) -> None:
        self.ply = 0
        self.started = False

    def set_clients(self, clients: dict[str, dict[str, Any]]) -> None:
        self.client_state.clients = clients
        for address, info in clients.items():
            if info["name"] == self.player.name:
                self.player.address = address
                break

    def generate_actions(self) -> None:
        self.allowed_acts = ActGroup([ActGroup(action.generate_allowed_acts()) for action in self.actions])
        # logger.debug(f"allowed: {self.allowed_acts}")
        # logger.debug(f"slots: {[str(slot) + f' [{i}]' for i, slot in enumerate(self.board.slots)]}")

    def set_config(self, config: Config, player_name: str = "") -> None:
        self.player.name = player_name
        self.config = config
        self.client_state.player_tiles = config.players

        self.actions = parse_actions(config.actions, self)

        self.board = BoardState(config.board)
        self.generate_actions()

    def is_player_turn(self, player_address: str | None = None) -> bool:
        player_number = self.player.number if not player_address \
            else next((player.number for player in self.players_in_game), -1)

        return self.ply % len(self.config.players) == player_number

    def get_tile_data(self, tile_expression: str) -> TileData:
        tile_parts = reversed(tile_expression.split("-"))
        name = next(tile_parts)
        group = next(tile_parts, "tiles")

        if name == "tile" and group == "player":
            name = self.current_player.tile
            group = "tiles"
        elif group == "player":
            group = self.current_player.tile

        return self.config.tiles.tile_data(name, group)


class BoardState:
    def __init__(self, board: BoardConfig | None):
        self.ui: BaseBoard | None = None
        self.slots = []

        if board and board.initialized:
            for slot in board.initial_slots:
                self.slots.append([state.get_tile_data(tile) for tile in slot])

    def handle_press(self, slot_index: int) -> tuple[int, str] | None:
        # todo: add player check
        if acts := state.allowed_acts.consume(slot_index):
            return slot_index, Act.list_as_str(acts)
        return None

    def init_board_ui(self, board_ui: BaseBoard) -> None:
        self.ui = board_ui
        # todo: get correct board view slots
        board_ui.setup_board(self.slots.copy(), state.config.board.color)

    def do_acts(self, acts_raw: list[list[Any]] | list[Act]) -> None:
        for act_raw in acts_raw:
            tile, slot_index, action_level, replace = (astuple(act_raw) if isinstance(act_raw, Act) else act_raw)
            tile_data = state.get_tile_data(tile)

            if (level := get_slot_level(len(self.slots[slot_index]), action_level, replace)) is not None:
                if self.ui:
                    # todo: get correct board view index
                    view_slot_index = slot_index
                    self.ui.update_board(tile_data, view_slot_index, level, replace)

                self.update(tile_data, slot_index, level, replace)

        state.generate_actions()

    def update(self, tile_data: TileData, slot_index: int, level: int, replace: bool) -> None:
        if replace:
            if tile_data.name == "":
                self.slots[slot_index].pop(level)
            else:
                self.slots[slot_index][level] = tile_data
        else:
            self.slots[slot_index].insert(level, tile_data)


class BaseBoard(Protocol):
    def setup_board(self, slots: list[list[TileData]], color: str) -> None: pass
    def update_board(self, tile: TileData, slot_index: int, level: int, replace: bool) -> None: pass


# Create global state object
state: Final[State] = State()
