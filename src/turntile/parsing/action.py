# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
# 
# This file is part of turntile.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from __future__ import annotations  # for type hinting forward references

import json
from abc import ABC, abstractmethod
from dataclasses import dataclass, astuple, field
from typing import Any, Protocol, TYPE_CHECKING

if TYPE_CHECKING:
    from turntile.parsing.config import TileData
    from turntile.state import State

from turntile.parsing.parsers import tile_list_parser

LEVEL_MAPPING = {
    "same": 0,
    "bottom": 1,
    "top": -1
}

TILE_LIST_PARSER = tile_list_parser(",", optional=True)


def get_tile_list(tile_list_string: str) -> list[str]:
    group: list[list[str]] = TILE_LIST_PARSER.parse_string(tile_list_string).as_list()
    return group[0]


def parse_level(level_text: str) -> int:
    if (level := LEVEL_MAPPING.get(level_text, None)) is not None:
        return level
    return int(level_text)


def get_slot_level(levels: int, action_level: int, replace: bool) -> int | None:
    if levels == 0 and replace:
        return None

    level_from_top = levels + action_level
    if action_level > levels or (level_from_top < 0):
        level = levels
    elif action_level > 0:
        level = action_level
    else:
        level = level_from_top

    if level == levels and replace:
        level -= 1

    return level


def parse_actions(action_config: dict[str, Any], state: State, single_action: bool = False) -> list[BaseAction]:
    actions: list[BaseAction] = []
    for action_type_key, config in action_config.items():
        actions.extend(ACTION_MAPPING[action_type_key.split(":")[-1]].expand(config, state))

        if single_action:
            break

    return actions


class BaseAction(ABC):
    def __init__(self, config: dict[str, Any], state: State):
        self.state = state
        self.conditions = self.parse_conditions(config)
        self.action_level = parse_level(config.get("level", "same"))

        chain_action = parse_actions(config.get("chain action", {}), state, single_action=True)
        self.chain: BaseAction | None = next(iter(chain_action), None)

    def __repr__(self) -> str:
        return f"{self.__dict__!r}"

    def parse_conditions(self, config: dict[str, Any]) -> list[list[BaseCondition]]:
        base_tiles = get_tile_list(config.get("base tile", ""))
        stop_tiles = get_tile_list(config.get("stop tile", ""))
        condition_config = config.get("condition", None)

        return [[TileCondition(self.state.get_tile_data(tile)) for tile in base_tiles]]

    @classmethod
    def expand(cls, config: dict[str, Any], state: State) -> list[BaseAction]:
        return [cls(config, state)]

    @abstractmethod
    def generate_allowed_acts(self) -> list[Act | ActGroup]:
        pass

    def slot_passes_all_conditions(self, slot: list[TileData]) -> bool:
        for and_conditions in self.conditions:
            or_is_valid = False
            for or_condition in and_conditions:
                if or_condition.check(slot):
                    or_is_valid = True
                    break

            if not or_is_valid:
                return False

        return True


class PlaceAction(BaseAction):
    def generate_allowed_acts(self) -> list[Act | ActGroup]:
        return [
            Act(self.placed_tile, slot_index, self.action_level)
            for slot_index, slot in enumerate(self.state.board.slots)
            if self.slot_passes_all_conditions(slot)
        ]

    def __init__(self, config: dict[str, Any], state: State) -> None:
        self.placed_tile = config["placed tile"]
        super().__init__(config, state)

        # Ensure placing directly on top
        if self.action_level == -1:
            self.action_level = 0


class ReplaceAction(BaseAction):
    def generate_allowed_acts(self) -> list[Act | ActGroup]:
        return [
            Act(self.placed_tile, slot_index, len(slot), replace=True)
            for slot_index, slot in enumerate(self.state.board.slots)
            if self.slot_passes_all_conditions(slot)
        ]

    def __init__(self, config: dict[str, Any], state: State) -> None:
        self.placed_tile = config["placed tile"]
        super().__init__(config, state)


class RemoveAction(BaseAction):
    def generate_allowed_acts(self) -> list[Act | ActGroup]:
        return [
            Act("", slot_index, len(slot), replace=True)
            for slot_index, slot in enumerate(self.state.board.slots)
            if self.slot_passes_all_conditions(slot)
        ]


class MoveAction(BaseAction):
    def generate_allowed_acts(self) -> list[Act | ActGroup]:
        pass


ACTION_MAPPING: dict[str, type[BaseAction]] = {
    "place": PlaceAction,
    "replace": ReplaceAction,
    "remove": RemoveAction,
    "move": MoveAction,
}


class ActEncoder(json.JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, Act):
            return astuple(o)
        return o.__dict__()


@dataclass(slots=True)
class Act:
    tile_type: str
    slot_index: int
    level: int
    replace: bool = False

    @staticmethod
    def list_as_str(acts: list[Act]) -> str:
        return json.dumps(acts, cls=ActEncoder, separators=(",", ":"))

    def __repr__(self) -> str:
        return f"{self.tile_type} [{self.slot_index}]"


@dataclass(slots=True)
class ActGroup:
    acts: list[Act | ActGroup] = field(default_factory=list)

    def consume(self, slot_index: int) -> list[Act]:
        acts = []
        for act in self.acts:
            if isinstance(act, Act):
                if act.slot_index == slot_index:
                    acts.append(act)
                    break  # todo: find all matching
            elif len(act.acts) > 0:
                if (sub_acts := act.consume(slot_index)) and len(sub_acts) > 0:
                    acts.append(sub_acts[0])
                    break
        return acts

    def __repr__(self) -> str:
        return str(self.acts)


class BaseCondition(Protocol):
    def check(self, slot: list[TileData]) -> bool:
        pass


@dataclass
class TraversalCondition:
    shape: Shape
    direction: str
    range: int
    base: str
    stop: str

    def check(self, slot: list[TileData]) -> bool:
        pass


@dataclass
class Shape:
    type: str
    options: tuple[int, ...] | None


@dataclass
class TileCondition:
    tile_data: TileData

    def check(self, slot: list[TileData]) -> bool:
        top_tile = slot[-1:]
        return len(top_tile) > 0 and self.tile_data == top_tile[0]
