# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
# 
# This file is part of turntile.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from __future__ import annotations  # for type hinting forward references

from typing import Any

import pyparsing as pp
from pyparsing import pyparsing_common as ppc
from strictyaml import Str, Seq, CommaSeparated, MapCombined, NullNone, Map, Int, Any as Anything, Optional


def list_parser(item_validator: Any) -> Any:
    return Seq(item_validator) | CommaSeparated(item_validator)


def property_parser(properties: dict[Any, Any], allow_any: bool = False) -> Any:
    return MapCombined(properties, Str(), Anything() if allow_any else NullNone())


CONFIG_SCHEMA = Map(
    {
        "name": Str(),
        "description": Str(),
        "players": list_parser(Str()),  # | Int(),   int not yet supported

        Optional("board"): property_parser(
            {
                Optional("size"): list_parser(Int()),
                Optional("columns"): list_parser(Str()),
                Optional("rows"): list_parser(Str()),
                Optional("geometry", default="square"): Str(),
                Optional("color", default="#000000"): Str(),
                Optional("view", default="same"): Str(),
                Optional("fill"): property_parser({
                    Optional("all"): Str(),
                    Optional("checker"): list_parser(Str()),
                    Optional("manual"): Anything(),
                }),
            }
        ),

        Optional("actions"): Anything()
    }
)


def index_range_parser() -> Any:
    return (
            ppc.integer +
            pp.Suppress("-") +
            ppc.integer
    ).set_parse_action(lambda t: list(range(t[0], t[1] + 1)))


def board_fill_indexing_parser() -> Any:
    return (index_range_parser() | ppc.integer | pp.Opt(pp.Word(pp.alphanums + "-"), default=""))("indexing")


def tile_list_parser(separator: str = "/", optional: bool = False) -> Any:
    parser = pp.Group(pp.delimited_list(pp.Word(pp.alphanums + "-"), delim=separator), aslist=True).set_name("tile-list")
    return pp.Opt(parser, default=[""]) if optional else parser


def traversal_parser(default_base: str, default_stop: str) -> Any:
    shape = pp.Opt(pp.Group(pp.delimited_list(
        pp.Keyword("orthogonal") |
        pp.Keyword("vertical") |
        pp.Keyword("horizontal") |
        pp.Keyword("diagonal") |
        pp.Keyword("square") | pp.Keyword("all") |
        pp.Keyword("rectangle") |
        pp.Keyword("circle") |
        pp.Group(pp.Keyword("vector") + pp.Suppress("-") +
                 pp.Group(ppc.integer + pp.Suppress(":") + ppc.integer), aslist=True)
        , delim="/").set_name("shape")), [])

    indexes = pp.Opt(
        pp.Group(pp.delimited_list(
            (index_range_parser() | ppc.signed_integer)
            , delim="/").set_name("indexes")), [])

    direction = pp.Opt(pp.Group(pp.delimited_list(
        pp.Keyword("leftward") |
        pp.Keyword("left") |
        pp.Keyword("rightward") |
        pp.Keyword("right") |
        pp.Keyword("upward") |
        pp.Keyword("up") |
        pp.Keyword("forward") |
        pp.Keyword("downward") |
        pp.Keyword("down") |
        pp.Keyword("backward")
        , delim="/").set_name("direction")), [])

    tile_list = tile_list_parser()
    base_tile = pp.Opt((pp.Keyword("on-base").suppress() + tile_list).set_name("base-tile"),
                       [default_base] if default_base else [])
    stop_tile = pp.Opt((pp.Keyword("stop-on").suppress() + tile_list).set_name("stop-tile"),
                       [default_stop] if default_stop else [])

    identifier = pp.Opt((pp.Suppress("[") + pp.Word(pp.alphanums + "-") + pp.Suppress("]")).set_name("identifier"), "")

    return (
            pp.Keyword("on") +
            shape("shape") +
            direction("direction") +
            indexes("indexes") +
            base_tile("base-tile") +
            stop_tile("stop-tile") +
            identifier("identifier")
    ).set_name("traversal")


def condition_parser(default_base: str, default_stop: str) -> Any:
    return (traversal_parser(default_base, default_stop) | pp.Keyword("tile") + tile_list_parser())\
        .set_name("condition")
    