# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
# 
# This file is part of turntile.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from __future__ import annotations  # for type hinting forward references

import argparse
import base64
import json
import os
from dataclasses import dataclass, field
from pathlib import Path
from typing import Any

from strictyaml import load

from turntile.log import logger
from turntile.parsing.board import BoardConfig
from turntile.parsing.parsers import CONFIG_SCHEMA


def parse_arguments() -> tuple[str, bool, int, str | None, str, str, str, bool]:
    arguments = argparse.ArgumentParser(description="A custom YAML format interpreter for defining turn-based "
                                                    "singleplayer/multiplayer games that are played on boards "
                                                    "consisting of image tiles.")
    arguments.add_argument("--log-level", help="Logging level", default="info")

    client_arguments = arguments.add_argument_group('client')
    client_arguments.add_argument("--backend", help="GUI backend (default turntile-kivy)",
                                  default="turntile-kivy", type=str)
    client_arguments.add_argument("--player-name", help="Default player name", default="", type=str)
    client_arguments.add_argument("--connect-address", help="Automatically connect to this server address "
                                                            "(Player name default must also be specified)",
                                  default="", type=str)
    client_arguments.add_argument("--auto-assign", help="Assign player automatically", action="store_true")

    server_arguments = arguments.add_argument_group('server')
    server_arguments.add_argument("--server", help="Start a server", action="store_true")
    server_arguments.add_argument("--port", "-p", help="Server port", type=int, default=7671)
    server_arguments.add_argument("--directory", "-d", help="The interface definition directory", type=str)

    args = arguments.parse_args()
    return args.log_level, args.server, args.port, args.directory, args.backend.replace("-", "_"), \
        args.player_name, args.connect_address, args.auto_assign


def parse_config(directory: str | None) -> Config | None:
    yaml_path = validate_path(directory)
    if yaml_path is None:
        return None

    configuration = load(concat_files_from_path(yaml_path), schema=CONFIG_SCHEMA).data

    tile_path = yaml_path.joinpath("tiles")
    if tile_path.is_dir():
        configuration["tiles"] = path_as_dict(tile_path)

    logger.debug(json.dumps(configuration, indent=2))
    return Config(configuration)


class Config:
    def __init__(self, configuration: dict[str, Any] | None = None) -> None:
        if not configuration:
            configuration = {}
        self.configuration = configuration

        self.tiles: TileDataGroup = TileDataGroup.from_config(configuration.get("tiles", {}))
        self.players: list[str] = configuration.get("players", None)

        self.name: str = configuration.get("name", "")
        self.description: str = configuration.get("description", "")

        self.board: BoardConfig = BoardConfig(configuration.get("board", {}))
        self.actions: dict[str, Any] = configuration.get("actions", {})


def dict_pairs_recursive(dictionary: dict[Any, Any], parents: list[str]) -> list[Any]:
    pairs: list[Any] = []
    for key, value in dictionary.items():
        if isinstance(value, dict):
            pairs.extend(dict_pairs_recursive(value, [key] + parents))
        else:
            pairs.append((key, value, parents))
    return pairs


def validate_path(directory: str | None) -> Path | None:
    # currently exit if no game definition given as an argument
    # (could also show selection screen where file could be selected) (given through client?)
    if directory is None:
        logger.error("No interface definition given! Exiting...")
        return None

    yaml_path = Path(directory).resolve()

    if not yaml_path.is_dir():
        logger.error(f"Invalid directory: {yaml_path}")
        return None

    return yaml_path


def concat_files_from_path(path: Path) -> str:
    concat: list[str] = []

    for entry in path.iterdir():
        if os.path.isfile(entry):
            with open(entry, "r") as file:
                return file.read() + "\n"

    return "".join(concat)


def path_as_dict(path: Path | str) -> dict[str, Any]:
    for root, dirs, files in os.walk(path):
        tree: dict[str, Any] = {}
        for directory in dirs:
            tree[f"{directory}"] = path_as_dict(os.path.join(root, directory))
        for file in files:
            with open(os.path.join(root, file), "rb") as file_bytes:
                tree[f"{os.path.splitext(file)[0]}"] = base64.b85encode(file_bytes.read()).decode("utf-8")
        return tree

    return {}


@dataclass(slots=True)
class TileData:
    name: str
    data: bytes = b""
    groups: list[str] = field(default_factory=list)
    single_color: bool = False

    @classmethod
    def from_b85(cls, name: str, b85: str, groups: list[str]) -> TileData:
        tile_bytes = base64.b85decode(b85.encode("utf-8"))
        return cls(name, tile_bytes, groups)

    def __repr__(self) -> str:
        return self.name


@dataclass
class TileDataGroup:
    name: str
    data: list[TileData] = field(default_factory=list)

    @classmethod
    def from_config(cls, state_tiles: dict[str, Any]) -> TileDataGroup:
        tile_pairs = dict_pairs_recursive(state_tiles, ["tiles"])
        return cls("tiles", [TileData.from_b85(name, b85, groups) for name, b85, groups in tile_pairs])

    def tile_data(self, name: str, group: str = "tiles") -> TileData:
        # if tile name is a hex color, return special value without group
        if name.startswith("#"):
            return TileData(name, single_color=True)

        # if no tile name, return empty tile (used as remove marker)
        if name == "":
            return TileData("")

        # get the first tile in specific group, or None if it cannot be found
        return next((tile for tile in self.data if tile.name == name
                    and (group == "tiles" or group in tile.groups)),

                    # return violet error tile if tile not found
                    TileData("#8F00FF", single_color=True))

    def sub_group(self, sub_group: str) -> TileDataGroup:
        # returns union group if groups with same names under different root
        return TileDataGroup(sub_group, [tile for tile in self.data if sub_group in tile.groups])


if __name__ == "__main__":
    config = parse_config("../../../examples/testing")
    assert config is not None and config.board is not None and config.board.initialized
    print(config.actions)
