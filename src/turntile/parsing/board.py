# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
# 
# This file is part of turntile.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from __future__ import annotations  # for type hinting forward references

from typing import Any
from turntile.parsing.parsers import board_fill_indexing_parser


def add_tile_with_prefix(slot: list[str], tile: str, tile_prefix: str) -> None:
    tile = tile.strip()
    if tile != ".":
        slot.append(tile_prefix + tile)


class BoardConfig:
    def __init__(self, board_config: dict[str, Any]):
        self.initialized = False
        if not board_config:
            return

        if size := board_config.get("size", None):
            self.columns, self.rows = [], []
            self.width, self.height = size
        else:
            self.columns, self.rows = board_config["columns"], board_config["rows"]
            self.width, self.height = len(self.columns), len(self.rows)

        self.color = board_config["color"]
        self.geometry = board_config["geometry"]
        self.view = board_config["view"]

        if self.geometry != "square":
            raise NotImplementedError("Other board geometry types not yet implemented (only square)")

        self.initial_slots = self.parse_fills(board_config)

        self.initialized = True

    def parse_fills(self, board_config: dict[str, Any]) -> list[list[str]]:
        initial_slots: list[list[str]] = [[] for _ in range(self.width * self.height)]
        if fill := board_config.get("fill", None):
            if tile := fill.get("all", None):
                for slot in initial_slots:
                    slot.append(tile)

            if checker := fill.get("checker", None):
                odd_width = self.width % 2 != 0
                for i, slot in enumerate(initial_slots):
                    slot.append(checker[i % 2 if odd_width or (i // self.width) % 2 == 0 else ((i + 1) % 2)])

            if manual := fill.get("manual", None):
                self.parse_manual_fill(manual, initial_slots)

        return initial_slots

    def parse_manual_fill(self, layer: dict[str, Any], initial_slots: list[list[str]], tile_prefix: str = "") -> None:
        indexing_parser = board_fill_indexing_parser()

        for key, values in layer.items():
            indexing = indexing_parser.parse_string(key).indexing.as_list()
            elements = values.split(",") if type(values) == str else values

            if (row_index := self.rows.index(key) if key in self.rows else None) is not None:
                for i, tile in enumerate(elements):
                    add_tile_with_prefix(initial_slots[i + row_index * len(self.rows)], tile, tile_prefix)
            elif (col_index := self.columns.index(key) if key in self.columns else None) is not None:
                for i, tile in enumerate(elements):
                    add_tile_with_prefix(initial_slots[i * len(self.columns) + col_index], tile, tile_prefix)
            elif type(indexing[0]) == int:
                if len(elements) == 1 and len(indexing) > 1 and type(elements) == list:
                    elements *= len(indexing)

                for index, tile in zip(indexing, elements, strict=True):
                    add_tile_with_prefix(initial_slots[index - 1], tile, tile_prefix)
            else:
                self.parse_manual_fill(layer[key], initial_slots, indexing[0] + "-")
