# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import sys

from turntile.log import logger
from turntile import main


rc = 1
try:
    rc = main()
except Exception as e:
    logger.critical(str(e))
    raise

sys.exit(rc)
