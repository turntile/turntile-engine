# Copyright (C) 2022, Teemu Lehti <temeleh@pm.me>
#
# This file is part of turntile.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
def main() -> int:
    # import locally to prevent polluting __init__
    from turntile.parsing.config import parse_config, parse_arguments
    from turntile.networking.network import NetworkHandler, network
    from turntile.log import logger, setup_logging
    import importlib

    log_level, server, port, directory, backend_name, player_name, connect_address, auto_assign = parse_arguments()

    setup_logging(log_level.upper())
    logger.debug("Using debug logging")

    if server:
        config = parse_config(directory)
        if config is None:
            return 1

        network.start_server(port, config)
    else:
        if backend_name == "turntile_kivy":
            import os
            os.environ["KIVY_NO_ARGS"] = "1"

        backend = importlib.import_module(backend_name)
        network.start_client(backend.InterfaceHandler(port, player_name, connect_address, auto_assign))

    return 0


if __name__ == "__main__":
    from turntile.log import logger
    import sys

    rc = 1
    try:
        rc = main()
    except Exception as e:
        logger.critical(str(e))
        raise

    sys.exit(rc)
