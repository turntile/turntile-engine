import threading
from queue import Queue
from typing import Any

import pytest

from turntile.networking.network import ServerHandler, NetworkHandler
from turntile.networking.util import BaseInterfaceHandler, ScreenType, MessageType, PAYLOAD_BYTE_COUNT, \
    PAYLOAD_MAX_SIZE, LargePayloadException
from turntile.parsing.config import Config


class InterfaceHandlerTest(BaseInterfaceHandler):
    messages = Queue()

    def start_client(self) -> None:
        pass

    def set_clients(self, clients: dict[str, dict[str, Any]]) -> None:
        self.messages.put(clients)

    def init_config(self, configuration: dict[str, Any]) -> None:
        self.messages.put(configuration)

    def do_acts(self, update: dict[str, Any]) -> None:
        self.messages.put(update)

    def open_game(self, player_name: str) -> None:
        self.messages.put(player_name)

    def set_screen(self, screen_type: ScreenType) -> None:
        self.messages.put(screen_type)

    def show_message(self, message: str) -> None:
        self.messages.put(message)

    def next_turn(self, ply: int) -> None:
        self.messages.put(ply)


@pytest.fixture(scope="module")
def interface():
    return InterfaceHandlerTest(12345)


@pytest.fixture(scope="module")
def network_handlers(interface):
    configuration = {"name": "test", "description": ""}

    server = NetworkHandler()
    threading.Thread(target=lambda: server.start_server(12345, Config(configuration)), daemon=True).start()

    client = NetworkHandler()
    client.start_client(interface)
    client.connect_to_server("localhost", 12345, None)
    yield server, client

    server.close_server()


def test_server_client_connected(network_handlers):
    server, client = network_handlers
    assert len(client.connections) == 1 and len(server.connections) == 1


@pytest.mark.parametrize("msg_type, payload", [
    (MessageType.SET_SCREEN, ScreenType.LOBBY),
    (MessageType.SHOW_MESSAGE, "Message!"),
])
def test_server_send_client_receive_messages(network_handlers, interface, msg_type, payload):
    server, client = network_handlers
    server.send(msg_type, payload)
    assert interface.messages.get(True, 2) == payload


@pytest.mark.parametrize("payload_size",
                         (2 ** (8 * PAYLOAD_BYTE_COUNT),
                          2 ** (8 * PAYLOAD_BYTE_COUNT) + 1,
                          2 ** (8 * PAYLOAD_BYTE_COUNT) - 1)
                         )
def test_send_large_message(network_handlers, payload_size):
    server, client = network_handlers
    if payload_size > PAYLOAD_MAX_SIZE:
        with pytest.raises(LargePayloadException):
            server.send(MessageType.DEBUG, bytes(payload_size))
    else:
        server.send(MessageType.DEBUG, bytes(payload_size))


def test_unique_message_types():
    types = MessageType.__members__.values()
    assert len(types) == len(set(types))


def test_are_all_message_types_handled():
    for msg_type in MessageType.__members__.values():
        if 10 <= msg_type.value < 50:
            assert getattr(BaseInterfaceHandler, msg_type.name.lower())
        elif msg_type.value >= 50:
            assert getattr(ServerHandler, msg_type.name.lower())
