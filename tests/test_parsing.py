from pathlib import Path
import pytest

from turntile.parsing.config import parse_config, validate_path


@pytest.mark.parametrize("file, valid", [
    (None, False),
    ("none", False),
    ("examples/tictactoe/", True),
    ("examples/tictactoe", True),
    ("examples/tictactoe/tictactoe.yaml", False),
])
def test_valid_file_paths(file, valid):
    path = validate_path(file)
    if valid:
        assert isinstance(path, Path)
    else:
        assert path is None


@pytest.mark.parametrize("file, tiles_len, slots", [
    ("examples/tictactoe", 3, [["empty"], ["empty"], ["empty"], ["empty"], ["empty"], ["empty"], ["empty"], ["empty"], ["empty"]]),
    ("examples/chess", 12, [['#DEE3E6', 'black-rook'], ['#8CA2AD', 'black-knight'], ['#DEE3E6', 'black-bishop'], ['#8CA2AD', 'black-queen'], ['#DEE3E6', 'black-king'], ['#8CA2AD', 'black-bishop'], ['#DEE3E6', 'black-knight'], ['#8CA2AD', 'black-rook'], ['#8CA2AD', 'black-pawn'], ['#DEE3E6', 'black-pawn'], ['#8CA2AD', 'black-pawn'], ['#DEE3E6', 'black-pawn'], ['#8CA2AD', 'black-pawn'], ['#DEE3E6', 'black-pawn'], ['#8CA2AD', 'black-pawn'], ['#DEE3E6', 'black-pawn'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#8CA2AD'], ['#DEE3E6'], ['#DEE3E6', 'white-pawn'], ['#8CA2AD', 'white-pawn'], ['#DEE3E6', 'white-pawn'], ['#8CA2AD', 'white-pawn'], ['#DEE3E6', 'white-pawn'], ['#8CA2AD', 'white-pawn'], ['#DEE3E6', 'white-pawn'], ['#8CA2AD', 'white-pawn'], ['#8CA2AD', 'white-rook'], ['#DEE3E6', 'white-knight'], ['#8CA2AD', 'white-bishop'], ['#DEE3E6', 'white-queen'], ['#8CA2AD', 'white-king'], ['#DEE3E6', 'white-bishop'], ['#8CA2AD', 'white-knight'], ['#DEE3E6', 'white-rook']]),
])
def test_basic_config_parsing(file, tiles_len, slots):
    config = parse_config(file)

    assert slots == config.board.initial_slots
    assert len(config.tiles.data) == tiles_len
